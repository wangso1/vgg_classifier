#!/bin/bash

# - This script is to be run in a clean Ubuntu 14.04 LTS machine, by a sudoer user.
# - Caffe is compiled for CPU use only.

# update repositories
sudo apt-get update

# Caffe  dependencies
sudo apt-get install -y cmake
sudo apt-get install -y pkg-config
sudo apt-get install -y libgoogle-glog-dev
sudo apt-get install -y libhdf5-serial-dev
sudo apt-get install -y liblmdb-dev
sudo apt-get install -y libleveldb-dev
sudo apt-get install -y libprotobuf-dev
sudo apt-get install -y protobuf-compiler
sudo apt-get install -y libopencv-dev
sudo apt-get install -y libatlas-base-dev
sudo apt-get install -y libsnappy-dev
sudo apt-get install -y libgflags-dev
sudo apt-get install -y --no-install-recommends libboost-all-dev

# pip and python dependencies
sudo apt-get install -y python-pip
sudo apt-get install -y python-dev

# download gitlab repo
sudo apt-get install -y wget unzip
cd $HOME
wget https://gitlab.com/vgg/vgg_classifier/repository/archive.zip?ref=master -O /tmp/vgg_classifier.zip
unzip /tmp/vgg_classifier.zip
mv vgg_classifier* vgg_classifier
rm -rf /tmp/vgg_classifier.zip

git clone https://gitlab.com/vgg/vgg_classifier.git

# setup folders
mkdir $HOME/vgg_classifier/dependencies
mkdir $HOME/vgg_classifier/downloaded

# caffe-backend aditional dependencies
sudo apt-get install -y libzmq-dev
sudo pip install protobuf==3.0.0
# liblinear installed below is also a dependency.
# caffe installed below is also a dependency.
# cpp-netlib installed below is also a dependency.

# cpp-netlib aditional dependencies
sudo apt-get install -y libssl-dev

# download caffe
wget https://github.com/BVLC/caffe/archive/rc3.zip
unzip rc3.zip -d $HOME/vgg_classifier/dependencies/

# download cpp-netlib
wget https://github.com/kencoken/cpp-netlib/archive/0.11-devel.zip
unzip 0.11-devel.zip -d $HOME/vgg_classifier/dependencies/

# download liblinear
wget https://github.com/cjlin1/liblinear/archive/v210.zip
unzip v210.zip -d $HOME/vgg_classifier/dependencies/

# remove zips
rm 0.11-devel.zip rc3.zip v210.zip

# compile caffe
cd $HOME/vgg_classifier/dependencies/caffe-rc3/
cp Makefile.config.example Makefile.config
sed -i 's/# CPU_ONLY/CPU_ONLY/g' Makefile.config
sed -i 's/\/usr\/include\/python2.7/\/usr\/include\/python2.7 \/usr\/local\/lib\/python2.7\/dist-packages\/numpy\/core\/include/g' Makefile.config
make all

# compile cpp-netlib
cd  $HOME/vgg_classifier/dependencies/cpp-netlib-0.11-devel/
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_COMPILER=/usr/bin/cc -DCMAKE_CXX_COMPILER=/usr/bin/c++ ../
make

# compile liblinear
cd $HOME/vgg_classifier/dependencies/liblinear-210/
make lib
ln -s liblinear.so.3 liblinear.so

# vgg_classifier additional dependencies
sudo apt-get install -y libevent-dev
sudo pip install greenlet==0.4.10 gevent==0.13.8
sudo pip install gevent-zeromq==0.2.5
sudo pip install matplotlib==1.5.3

# additional dependencies for vgg_classifier test scripts
#imsearch-tools module, see https://github.com/kencoken/imsearch-tools

# compile and install vgg_classifier
cd $HOME/vgg_classifier
mkdir build
cd build
cmake -DCaffe_DIR=$HOME/vgg_classifier/dependencies/caffe-rc3/ -DCaffe_INCLUDE_DIR="$HOME/vgg_classifier/dependencies/caffe-rc3/include;$HOME/vgg_classifier/dependencies/caffe-rc3/build/src" -DLiblinear_DIR=$HOME/vgg_classifier/dependencies/liblinear-210/ -Dcppnetlib_DIR=$HOME/vgg_classifier/dependencies/cpp-netlib-0.11-devel/build/ ../
make
make install

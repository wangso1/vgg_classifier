vgg_classifier Installers
=========================

These installers are experimental. Please use caution when using them and read this README until the end before attempting the installation.

Ubuntu 14 LTS
-------------

 1. This script is to be run in a clean Ubuntu 14.04 LTS machine, by a sudoer user.
 2. Caffe is compiled for CPU use only.
 3. `vgg_classifier` will be installed in the $HOME/vgg_classifier folder.
 4. Should you like to run the tests, you will need the `imsearch-tools` module (https://github.com/kencoken/imsearch-tools).
 5. Remember that before the first use you need to configure `vgg_classifier`. See the `Usage` section of the README in the root folder of this repository.

macOS Sierra v10.12.3
---------------------

 1. This script is VERY EXPERIMENTAL. Please be carefull. Instead of running the full script, you might want to open it in a text editor and run one instruction at a time.
 2. The script assumes Homebrew is available in the system (https://brew.sh/).
 3. The script assumes GIT is installed (https://sourceforge.net/projects/git-osx-installer/files/).
 4. Make sure you have enough user priviledges to install software using HomeBrew.
 5. Caffe is compiled for CPU use only.
 6. `vgg_classifier` will be installed in the $HOME/vgg_classifier folder.
 7. Should you like to run the tests, you will need the `imsearch-tools` module (https://github.com/kencoken/imsearch-tools)
 8. Remember that before the first use you need to configure `vgg_classifier`. See the `Usage` section of the README in the root folder of this repository.
